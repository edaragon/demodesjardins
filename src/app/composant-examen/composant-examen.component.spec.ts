import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ComposantExamenComponent } from './composant-examen.component';

describe('ComposantExamenComponent', () => {
  let component: ComposantExamenComponent;
  let fixture: ComponentFixture<ComposantExamenComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [ComposantExamenComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(ComposantExamenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
