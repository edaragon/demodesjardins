export interface demoComponent {
    title: string;
    hideActions?: boolean;
    actions?: [demoComponentAction, demoComponentAction];
    libelles?: demoComponentAction[];
    slideToggleOptions?: demoComponentAction;
  }
  
  export interface demoComponentAction<T = void> {
    title: string;
    action: (action: demoComponentAction<T>) => T;
    isDisable?: boolean;
    isHidden?: boolean;
  }