import { Component, EventEmitter, Input, OnChanges, Output, SimpleChanges } from '@angular/core';
import { takeUntilDestroyed } from '@angular/core/rxjs-interop';
import { MatButtonModule } from '@angular/material/button';
import { MatMenuModule } from '@angular/material/menu';
import {MatIconModule} from '@angular/material/icon';
import { CommonModule } from '@angular/common';
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import {MatSlideToggleModule} from '@angular/material/slide-toggle';
import { FormControl, FormGroup, ReactiveFormsModule } from '@angular/forms';
import { distinctUntilChanged, map, skip } from 'rxjs';
import { demoComponent } from './demoComponent';

@Component({
  selector: 'app-composant-examen',
  standalone: true,
  imports: [MatButtonModule, MatMenuModule, MatIconModule, CommonModule, MatButtonToggleModule, MatSlideToggleModule, ReactiveFormsModule],  
  templateUrl: './composant-examen.component.html',
  styleUrl: './composant-examen.component.scss'
})
export class ComposantExamenComponent implements OnChanges{
  @Input({required: true}) title: demoComponent['title'] = "";
  @Input() hideActions: demoComponent['hideActions'];
  @Input() actions: demoComponent['actions'];
  @Input() libelles: demoComponent['libelles'];
  @Input() slideToggleOptions?: demoComponent['slideToggleOptions'];

  @Output() resultForm: EventEmitter<Required<ReturnType<typeof this.form.getRawValue>>> = new EventEmitter();

  protected form = new FormGroup({
    libelle: new FormControl<string | undefined>(undefined),
    slideToggle: new FormControl<boolean>(false)
  })

  constructor(){
    this.form.valueChanges.pipe(
      takeUntilDestroyed(),
      skip(1),
      distinctUntilChanged(),
      map(values => this.resultForm.emit(this.form.getRawValue()))
    ).subscribe();
  }

  ngOnChanges(changes: SimpleChanges){
    this.form.get('slideToggle')?.[this.slideToggleOptions?.isDisable ? 'disable' : 'enable']();  
  }

  protected get showLibelle(){
    return !!this.libelles?.find(libelle => !libelle.isHidden);
  }

  protected onToggleButtonChange(libelleIndex: number){
    //TODO: implementer l'action du toggle button ici ou sur le valueChanges du form
    const targetLibelle = this.libelles && this.libelles[libelleIndex];
    targetLibelle?.action(targetLibelle);
  }
}
