import { Component } from '@angular/core';
import { RouterOutlet } from '@angular/router';
import { ComposantExamenComponent } from './composant-examen/composant-examen.component';
import { CommonModule } from '@angular/common';
import { demoComponent } from './composant-examen/demoComponent';

@Component({
  selector: 'app-root',
  standalone: true,
  imports: [RouterOutlet, ComposantExamenComponent, CommonModule],
  templateUrl: './app.component.html',
  styleUrl: './app.component.scss'
})
export class AppComponent {
  demoComponents: demoComponent[] = [{
    title: 'Ceci est un long titre qui dépasse de la ligne donc il faut tronquer',
    actions: [{
      title: 'Modifier',
      action: (action) =>{
        console.log("Action in component", action.title);
      }
    },{
      title: 'Supprimer',
      isDisable: true,
      action: (action) =>{
        console.log("Action in component", action.title);
      }
    }],
    libelles: [{
      title: 'libelle 1',
      action(action) {
        console.log("Action in component", action.title);
      },
    },{
      title: 'libelle 2',
      action(action) {
        console.log("Action in component", action.title);
      },
    }],
    slideToggleOptions: {
      title: 'Afficher les modifications',
      isDisable: true,
      action: (action) => {
        console.log("Action in component", action.title);
      },
    }
  },{
      title: "petit titre",
      hideActions: true,
      actions: [{
        title: 'Modifier',
        action: (action) =>{
          console.log("Action in component", action.title);
        }
      },{
        title: 'Supprimer',
        action: (action) =>{
          console.log("Action in component", action.title);
        }
      }]
  },{
    title: "moyen titre un peu plus long",
    actions: [{
      title: 'Modifier',
      action: (action) =>{
        console.log("Action in component", action.title);
      }
    },{
      title: 'Supprimer',
      isHidden: true,
      action: (action) =>{
        console.log("Action in component", action.title);
      }
    }]
  },{
    title: "grand grand grand grand grand grand grand grand grand grand titre",
    libelles: [{
      title: 'libelle 1',
      action(action) {
        console.log("Action in component", action.title);
      },
    },{
      title: 'libelle 2',
      isDisable: true,
      action(action) {
        console.log("Action in component", action.title);
      },
    },{
      title: 'libelle 3',
      isHidden: true,
      action(action) {
        console.log("Action in component", action.title);
      },
    },{
      title: 'libelle 4',
      action(action) {
        console.log("Action in component", action.title);
      },
    },{
      title: 'libelle 5',
      action(action) {
        console.log("Action in component", action.title);
      },
    }],
    actions: [{
      title: 'Modifier',
      action: (action) =>{
        console.log("Action in component", action.title);
      }
    },{
      title: 'Supprimer',
      action: (action) =>{
        console.log("Action in component", action.title);
      }
    }],
    slideToggleOptions: {
      title: 'Afficher les modifications',
      action: (action) => {
        console.log("Action in component", action.title);
      },
    }
  }];

  showResult(result: ReturnType<ComposantExamenComponent['form']['getRawValue']>){
    console.log("Result from app.component", result)
  }
}
